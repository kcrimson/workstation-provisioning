#!/usr/bin/env bash

apt update
apt install ansible git

# configure password master password ;)
secret-tool store --label=keepassxc app keepassxc